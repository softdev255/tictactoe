package com.titima.tictactoe;

import java.util.Scanner;

public class OXProgram {

    static char winner;
    static boolean isFinish = false;
    static int row, col;
    static Scanner kb = new Scanner(System.in);
    static char[][] table = {
        {'-', '-', '-'},
        {'-', '-', '-'},
        {'-', '-', '-'}};
    static char player = 'X';

    static void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    static void showTable() {
        System.out.println(" 123");
        for (int row = 0; row < table.length; row++) {
            System.out.print(row + 1);
            for (int col = 0; col < table[row].length; col++) {
                System.out.print(table[row][col]);
            }
            System.out.println("");
        }
    }

    static void showTurn() {
        System.out.println(player + " turn");
    }

    static void input() {
        while (true) {
            System.out.println("Please input Row Col : ");
            row = kb.nextInt() - 1;
            col = kb.nextInt() - 1;
            if (table[row][col] == '-') {
                table[row][col] = player;
                break;
            }
            System.out.println("Error: table at row and col is not empty!!!");
        }
    }

    static void chexkCol() {
        for (int row = 0; row < 3; row++) {
            if (table[row][col] != player) {
                return;
            }
        }
        isFinish = true;
        winner = player;
    }

    static void chexkRow() {
        for (int col = 0; col < 3; col++) {
            if (table[row][col] != player) {
                return;
            }
        }
        isFinish = true;
        winner = player;
    }

    static void checkLeft() {
        for (int row = 0; row < 3; row++) {
            for (int col = 0; col < 3; col++) {
                if (row == col && table[row][col] != player) {
                    return;
                }
            }
        }
        isFinish = true;
        winner = player;
    }

    static void checkRight() {
        int col = 2;
        int row = 0;
        for (int round = 0; round < 3; round++) {
            if (table[row][col] != player) {
                return;
            }
            col--;
            row++;
        }
        isFinish = true;
        winner = player;
    }

    static void checkDraw() {
        if (player == 9 && isFinish == false && winner == '-') {
            isFinish = true;
        }
    }

    static void checkWin() {
        chexkRow();
        chexkCol();
        checkRight();
        checkLeft();
        checkDraw();
    }

    static void switchPlayer() {
        if (player == 'X') {
            player = 'O';
        } else {
            player = 'X';
        }
    }

    static void showResult() {
        if (winner == '-') {
            System.out.println("Draw!!");
        } else {
            showTable();
            System.out.println("Player " + winner + " win");
        }
    }

    static void showBye() {
        System.out.println("Bye Bye ....");
    }

    public static void main(String[] args) {
        showWelcome();
        do {
            showTable();
            showTurn();
            input();
            checkWin();
            switchPlayer();
        } while (!isFinish);
        showResult();
        showBye();
    }
}
